# Recruitment test #

* Food ordering system (Java 8, JUnit, Mockito)
* Mine sweeper (Java 8, Groovy, Spock)
* SQL 



## Setup ##

After checkout, build with Maven:
```
mvn clean install
```

### Food ordering system ###
In **/target** directory run:
```
java -jar food-ordering-1.0-SNAPSHOT.jar
```
to launch the command-line interface.

### Mine sweeper ###
In **/target** directory run, with sample input:
```
java -jar mine-sweeper-1.0-SNAPSHOT.jar "*..\n...\n*.*"
```
to expect:
```
*10
231
*2*
```