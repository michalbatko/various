package pl.mbatko;

import pl.mbatko.dialog.DefaultDialog;
import pl.mbatko.menu.DefaultMenu;
import pl.mbatko.order.OrderingSystem;

public class Solution {

    public static void main(String[] args) {
        OrderingSystem system = new OrderingSystem(new DefaultDialog(), new DefaultMenu());
        system.collectOrder();
    }
}
