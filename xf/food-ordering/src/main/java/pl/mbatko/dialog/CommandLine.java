package pl.mbatko.dialog;

import pl.mbatko.type.general.Selectable;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.String.*;

/**
 * Command-line communication with the user
 */
public class CommandLine {

    private Scanner scanner;

    public CommandLine() {
        this.scanner = new Scanner(System.in);
    }

    public CommandLine(String[] inputSequence) {
        this.scanner = new Scanner(String.join("\n", inputSequence));
    }

    public void message(String message) {
        System.out.println(message);
    }

    public boolean question(String question, String accept, String cancel) {
        List<String> options = Collections.singletonList(accept);
        String message = format("%s - type: %s or [%s] to skip", question, options, cancel);

        boolean selected = accept.equals(readAnswer(message, options, cancel));
        if (!selected) {
            message("Skipping");
        }

        return selected;
    }

    public <T extends Selectable> Optional<T> select(String question, T[] items, String cancel) {
        List<String> answers = Stream.of(items).map(Selectable::getCode).collect(Collectors.toList());

        StringJoiner description = new StringJoiner("\n");
        Stream.of(items).forEach(item -> description.add(format("  [%s] = %s", item.getCode(), item.getDescription())));

        String message = format("%s - type: %s or [%s] to skip\n%s", question, answers, cancel, description.toString());

        String selected = readAnswer(message, answers, cancel);
        if (selected.equals("/")) {
            message("Skipping");
            return Optional.empty();
        }

        Optional<T> first = Stream.of(items).filter(item -> item.getCode().equals(selected)).findFirst();
        message(format("Selected: %s", first.get().getDescription()));

        return first;
    }

    private String readAnswer(String message, List<String> answers, String cancel) {
        while (true) {
            message(message);

            String answer = scanner.nextLine();
            if (answer.equals(cancel) || answers.contains(answer)) {
                return answer;
            }
        }
    }
}
