package pl.mbatko.dialog;

import pl.mbatko.type.general.Selectable;

import java.util.Optional;

public class DefaultDialog implements Dialog {

    private static final String DEFAULT_ACCEPT = "y";
    private static final String DEFAULT_CANCEL = "/";

    private CommandLine commandLine;

    public DefaultDialog() {
        this.commandLine = new CommandLine();
    }

    public DefaultDialog(CommandLine commandLine) {
        this.commandLine = commandLine;
    }

    @Override
    public void message(String message) {
        commandLine.message(message);
    }

    @Override
    public boolean question(String question) {
        return commandLine.question(question, DEFAULT_ACCEPT, DEFAULT_CANCEL);
    }

    @Override
    public <T extends Selectable> Optional<T> select(String question, T[] items) {
        return commandLine.select(question, items, DEFAULT_CANCEL);
    }
}
