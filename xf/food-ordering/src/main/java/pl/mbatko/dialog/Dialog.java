package pl.mbatko.dialog;

import pl.mbatko.type.general.Selectable;

import java.util.Optional;

public interface Dialog {

    void message(String message);

    boolean question(String question);

    <T extends Selectable> Optional<T> select(String question, T[] items);
}
