package pl.mbatko.dialog;

public class DialogConstants {
    public static final String WELCOME = "Dear Guest, welcome!";
    public static final String NEXT_DRINK = "Something to drink?";
    public static final String SELECT_DRINK = "Please select your drink";
    public static final String NEXT_DRINK_ADD_ON = "Any additions to drink?";
    public static final String SELECT_DRINK_ADD_ON = "Please select what to add";
    public static final String NEXT_MEAL = "Something to eat?";
    public static final String SELECT_CUISINE = "Please select cuisine";
    public static final String SELECT_COURSE = "Please select main course";
    public static final String SELECT_DESSERT = "Please select dessert";
}
