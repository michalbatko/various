package pl.mbatko.menu;

import pl.mbatko.type.course.CourseTypes;
import pl.mbatko.type.cuisine.CuisineTypes;
import pl.mbatko.type.dessert.DessertTypes;
import pl.mbatko.type.drink.DrinkAddOnTypes;
import pl.mbatko.type.drink.DrinkTypes;
import pl.mbatko.type.general.*;

import java.util.stream.Stream;

/**
 * All the food available in the system
 */
public class DefaultMenu implements Menu {

    private CuisineType[] cuisines = CuisineTypes.get();
    private DrinkType[] drinks = DrinkTypes.get();
    private DrinkAddOnType[] drinksAddOns = DrinkAddOnTypes.get();
    private CourseType[] courses = CourseTypes.get();
    private DessertType[] desserts = DessertTypes.get();

    @Override
    public DrinkType[] getDrinks() {
        return drinks;
    }

    @Override
    public DrinkAddOnType[] getDrinkAddOns() {
        return drinksAddOns;
    }

    @Override
    public CuisineType[] getCuisines() {
        return cuisines;
    }

    @Override
    public CourseType[] getCourses() {
        return courses;
    }

    @Override
    public DessertType[] getDesserts() {
        return desserts;
    }

    @Override
    public CourseType[] getCourses(CuisineType cuisine) {
        return Stream.of(courses).filter(course -> course.getCuisine().equals(cuisine)).toArray(CourseType[]::new);
    }

    @Override
    public DessertType[] getDesserts(CuisineType cuisine) {
        return Stream.of(desserts).filter(dessert -> dessert.getCuisine().equals(cuisine)).toArray(DessertType[]::new);
    }

    public static Selectable[] byCode(String code, Selectable[] selectables) {
        return Stream.of(selectables).filter(s -> s.getCode().equals(code)).toArray(Selectable[]::new);
    }
}
