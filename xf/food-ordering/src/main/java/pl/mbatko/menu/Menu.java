package pl.mbatko.menu;

import pl.mbatko.type.general.*;

public interface Menu {

    DrinkType[] getDrinks();

    DrinkAddOnType[] getDrinkAddOns();

    CuisineType[] getCuisines();

    CourseType[] getCourses();

    CourseType[] getCourses(CuisineType cuisine);

    DessertType[] getDesserts();

    DessertType[] getDesserts(CuisineType cuisine);

}
