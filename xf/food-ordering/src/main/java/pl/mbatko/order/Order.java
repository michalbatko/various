package pl.mbatko.order;

import pl.mbatko.other.Price;
import pl.mbatko.type.general.Course;
import pl.mbatko.type.general.Dessert;
import pl.mbatko.type.general.Drink;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class Order {

    private List<Drink> drinks = new ArrayList<>();

    private List<Course> courses = new ArrayList<>();

    private List<Dessert> desserts = new ArrayList<>();

    public void addDrink(Drink drink) {
        this.drinks.add(drink);
    }

    public void addCourse(Course course) {
        this.courses.add(course);
    }

    public void addDessert(Dessert dessert) {
        this.desserts.add(dessert);
    }

    public List<Drink> getDrinks() {
        return drinks;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public List<Dessert> getDesserts() {
        return desserts;
    }

    @Override
    public String toString() {
        StringJoiner summary = new StringJoiner("\n");
        Price total = Price.of(0.0);

        summary.add("Summary:\n Drinks:");
        drinks.forEach(drink -> {
            summary.add("  " + drink.getType().getName());
            drink.getAddOns().forEach(addOn -> summary.add("   + " + addOn.getDescription()));
            total.add(drink.getType().getPrice());
        });

        summary.add(" Main courses:");
        courses.forEach(course -> {
            summary.add("  " + course.getType().getName());
            total.add(course.getType().getPrice());
        });

        summary.add(" Desserts:");
        desserts.forEach(dessert -> {
            summary.add("  " + dessert.getType().getName());
            total.add(dessert.getType().getPrice());
        });
        summary.add("Total: " + total.toString());

        return summary.toString();
    }
}
