package pl.mbatko.order;

import pl.mbatko.dialog.Dialog;
import pl.mbatko.menu.Menu;
import pl.mbatko.type.general.*;

import java.util.Optional;

import static pl.mbatko.dialog.DialogConstants.*;

/**
 * Builds an order
 */
public class OrderingSystem {

    private Dialog dialog;
    private Menu menu;

    public OrderingSystem(Dialog dialog, Menu menu) {
        this.dialog = dialog;
        this.menu = menu;
    }

    public Order collectOrder() {
        Order order = new Order();

        welcome();
        selectDrinks(order);
        selectMeal(order);
        showSummary(order);

        return order;
    }

    private void welcome() {
        dialog.message(WELCOME);
    }

    private void showSummary(Order order) {
        dialog.message(order.toString());
    }

    private void selectDrinks(Order order) {
        while (dialog.question(NEXT_DRINK)) {
            Optional<DrinkType> drinkType = dialog.select(SELECT_DRINK, menu.getDrinks());
            if (drinkType.isPresent()) {
                Drink drink = Drink.create(drinkType.get());
                while (dialog.question(NEXT_DRINK_ADD_ON)) {
                    Optional<DrinkAddOnType> addOnType = dialog.select(SELECT_DRINK_ADD_ON, menu.getDrinkAddOns());
                    if (addOnType.isPresent()) {
                        drink.with(addOnType.get());
                    }
                }

                order.addDrink(drink);
            }
        }
    }

    private void selectMeal(Order order) {
        while (dialog.question(NEXT_MEAL)) {
            Optional<CuisineType> cuisineType = dialog.select(SELECT_CUISINE, menu.getCuisines());
            if (cuisineType.isPresent()) {
                CuisineType cuisine = cuisineType.get();

                Optional<CourseType> courseType = dialog.select(SELECT_COURSE, menu.getCourses(cuisine));
                if (courseType.isPresent()) {
                    order.addCourse(Course.create(courseType.get()));
                }

                Optional<DessertType> dessertType = dialog.select(SELECT_DESSERT, menu.getDesserts(cuisine));
                if (dessertType.isPresent()) {
                    order.addDessert(Dessert.create(dessertType.get()));
                }
            }
        }
    }
}
