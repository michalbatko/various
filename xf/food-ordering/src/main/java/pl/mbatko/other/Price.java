package pl.mbatko.other;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Represents money
 */
public class Price {

    private BigDecimal value;

    private Price(BigDecimal value) {
        this.value = value;
    }

    public static Price of(double value) {
        return new Price(new BigDecimal(value).setScale(2, BigDecimal.ROUND_HALF_EVEN));
    }

    public void add(Price price) {
        this.value = this.value.add(price.value);
    }

    @Override
    public String toString() {
        return value.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Price price = (Price) o;
        return Objects.equals(value, price.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
