package pl.mbatko.type.course;

import pl.mbatko.type.cuisine.PolishCuisine;
import pl.mbatko.type.general.CourseType;

public class Bigos extends CourseType {

    public Bigos() {
        super(PolishCuisine.get(), "b", "Bigos", 10.0);
    }
}
