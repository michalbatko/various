package pl.mbatko.type.course;

import pl.mbatko.type.cuisine.MexicanCuisine;
import pl.mbatko.type.general.CourseType;

public class Buritto extends CourseType {

    public Buritto() {
        super(MexicanCuisine.get(), "t", "Buritto", 15.0);
    }
}
