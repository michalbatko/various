package pl.mbatko.type.course;

import pl.mbatko.type.general.CourseType;


/**
 * Register new course types here...
 */
public class CourseTypes {

    private static final CourseType[] types = {
            new Bigos(), new Schabowy(), new Pizza(), new Pasta(), new Buritto(), new Tacos()};

    public static CourseType[] get() {
        return types;
    }
}
