package pl.mbatko.type.course;

import pl.mbatko.type.cuisine.ItalianCuisine;
import pl.mbatko.type.general.CourseType;

public class Pasta extends CourseType {

    public Pasta() {
        super(ItalianCuisine.get(), "a", "Pasta", 9.0);
    }
}
