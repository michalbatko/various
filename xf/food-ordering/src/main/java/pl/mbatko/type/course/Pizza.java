package pl.mbatko.type.course;

import pl.mbatko.type.cuisine.ItalianCuisine;
import pl.mbatko.type.general.CourseType;

public class Pizza extends CourseType {

    public Pizza() {
        super(ItalianCuisine.get(), "p", "Pizza", 12.0);
    }
}
