package pl.mbatko.type.course;

import pl.mbatko.type.cuisine.PolishCuisine;
import pl.mbatko.type.general.CourseType;

public class Schabowy extends CourseType {

    public Schabowy() {
        super(PolishCuisine.get(), "s", "Kotlet schabowy + kapusta", 15.0);
    }
}
