package pl.mbatko.type.course;

import pl.mbatko.type.cuisine.MexicanCuisine;
import pl.mbatko.type.general.CourseType;

public class Tacos extends CourseType {

    public Tacos() {
        super(MexicanCuisine.get(), "o", "Tacos", 8.0);
    }
}
