package pl.mbatko.type.cuisine;

import pl.mbatko.type.general.CuisineType;


/**
 * Register new cuisine types here...
 */
public class CuisineTypes {

    private static final CuisineType[] types = {
            PolishCuisine.get(), MexicanCuisine.get(), ItalianCuisine.get()};

    public static CuisineType[] get() {
        return types;
    }
}
