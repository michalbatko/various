package pl.mbatko.type.cuisine;

import pl.mbatko.type.general.CuisineType;

public class ItalianCuisine extends CuisineType {

    private static final ItalianCuisine instance = new ItalianCuisine();

    private ItalianCuisine() {
        super("i", "Italian");
    }

    public static ItalianCuisine get() {
        return instance;
    }
}
