package pl.mbatko.type.cuisine;

import pl.mbatko.type.general.CuisineType;

public class MexicanCuisine extends CuisineType {

    private static final MexicanCuisine instance = new MexicanCuisine();

    private MexicanCuisine() {
        super("m", "Mexican");
    }

    public static MexicanCuisine get() {
        return instance;
    }
}
