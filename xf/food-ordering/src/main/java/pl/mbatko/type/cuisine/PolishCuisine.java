package pl.mbatko.type.cuisine;

import pl.mbatko.type.general.CuisineType;

public class PolishCuisine extends CuisineType {

    private static final PolishCuisine instance = new PolishCuisine();

    private PolishCuisine() {
        super("p", "Polish");
    }

    public static PolishCuisine get() {
        return instance;
    }
}
