package pl.mbatko.type.dessert;

import pl.mbatko.type.cuisine.MexicanCuisine;
import pl.mbatko.type.general.DessertType;

public class Churros extends DessertType {

    public Churros() {
        super(MexicanCuisine.get(), "c", "Churros", 3.0);
    }
}
