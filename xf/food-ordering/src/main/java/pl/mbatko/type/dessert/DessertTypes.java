package pl.mbatko.type.dessert;

import pl.mbatko.type.general.DessertType;

/**
 * Register new dessert types here...
 */
public class DessertTypes {

    private static final DessertType[] types = {
            new Kremowka(), new Szarlotka(), new Tiramisu(), new Churros(), new OrangeFlan()};


    public static DessertType[] get() {
        return types;
    }
}
