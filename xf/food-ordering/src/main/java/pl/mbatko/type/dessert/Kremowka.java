package pl.mbatko.type.dessert;

import pl.mbatko.type.cuisine.PolishCuisine;
import pl.mbatko.type.general.DessertType;

public class Kremowka extends DessertType {

    public Kremowka() {
        super(PolishCuisine.get(), "k", "Kremowka", 5.0);
    }
}
