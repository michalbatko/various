package pl.mbatko.type.dessert;

import pl.mbatko.type.cuisine.MexicanCuisine;
import pl.mbatko.type.general.DessertType;

public class OrangeFlan extends DessertType {

    public OrangeFlan() {
        super(MexicanCuisine.get(), "o", "Orange Flan", 8.0);
    }
}
