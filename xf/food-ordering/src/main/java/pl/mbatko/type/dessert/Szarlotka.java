package pl.mbatko.type.dessert;

import pl.mbatko.type.cuisine.PolishCuisine;
import pl.mbatko.type.general.DessertType;

public class Szarlotka extends DessertType {

    public Szarlotka() {
        super(PolishCuisine.get(), "s", "Szarlotka", 8.0);
    }
}
