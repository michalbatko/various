package pl.mbatko.type.dessert;

import pl.mbatko.type.cuisine.ItalianCuisine;
import pl.mbatko.type.general.DessertType;

public class Tiramisu extends DessertType {

    public Tiramisu() {
        super(ItalianCuisine.get(), "t", "Tiramisu", 5.0);
    }
}
