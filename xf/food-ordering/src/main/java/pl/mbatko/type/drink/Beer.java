package pl.mbatko.type.drink;

import pl.mbatko.type.general.DrinkType;

public class Beer extends DrinkType {

    public Beer() {
        super("b", "Local beer, 500ml", 5.0);
    }
}
