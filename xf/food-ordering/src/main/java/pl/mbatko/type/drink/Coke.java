package pl.mbatko.type.drink;

import pl.mbatko.type.general.DrinkType;

public class Coke extends DrinkType {

    public Coke() {
        super("c", "Coke / Coca Cola, 330ml", 3.0);
    }
}
