package pl.mbatko.type.drink;

import pl.mbatko.type.general.DrinkAddOnType;

/**
 * Register new drink add-on types here...
 */
public class DrinkAddOnTypes {

    private static final DrinkAddOnType[] types = {
            new Ice(), new Lemon()};

    public static DrinkAddOnType[] get() {
        return types;
    }
}
