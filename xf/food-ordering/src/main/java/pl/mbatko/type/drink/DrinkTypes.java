package pl.mbatko.type.drink;

import pl.mbatko.type.general.DrinkType;

/**
 * Register new drink types here...
 */
public class DrinkTypes {

    private static final DrinkType[] types = {
            new Water(), new Coke(), new Beer()};

    public static DrinkType[] get() {
        return types;
    }
}
