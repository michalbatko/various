package pl.mbatko.type.drink;

import pl.mbatko.type.general.DrinkAddOnType;

public class Ice extends DrinkAddOnType {

    public Ice() {
        super("i", "Ice");
    }
}
