package pl.mbatko.type.drink;

import pl.mbatko.type.general.DrinkAddOnType;

public class Lemon extends DrinkAddOnType {

    public Lemon() {
        super("l", "Lemon");
    }
}
