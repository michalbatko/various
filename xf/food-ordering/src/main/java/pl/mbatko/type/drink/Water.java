package pl.mbatko.type.drink;

import pl.mbatko.type.general.DrinkType;

public class Water extends DrinkType {

    public Water() {
        super("w", "Mineral water, 330ml", 2.0);
    }
}
