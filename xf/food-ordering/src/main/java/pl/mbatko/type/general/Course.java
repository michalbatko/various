package pl.mbatko.type.general;

public class Course {

    private CourseType type;

    private Course(CourseType type) {
        this.type = type;
    }

    public static Course create(CourseType type) {
        return new Course(type);
    }

    public CourseType getType() {
        return type;
    }
}
