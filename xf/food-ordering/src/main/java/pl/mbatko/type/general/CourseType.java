package pl.mbatko.type.general;

public abstract class CourseType extends MealType {

    public CourseType(CuisineType cuisine, String code, String name, double price) {
        super(cuisine, code, name, price);
    }
}
