package pl.mbatko.type.general;

public abstract class CuisineType extends SelectableType {

    public CuisineType(String code, String description) {
        super(code, description);
    }
}
