package pl.mbatko.type.general;

public class Dessert {

    private DessertType type;

    private Dessert(DessertType type) {
        this.type = type;
    }

    public static Dessert create(DessertType type) {
        return new Dessert(type);
    }

    public DessertType getType() {
        return type;
    }
}
