package pl.mbatko.type.general;

public abstract class DessertType extends MealType {

    public DessertType(CuisineType cuisine, String code, String name, double price) {
        super(cuisine, code, name, price);
    }
}
