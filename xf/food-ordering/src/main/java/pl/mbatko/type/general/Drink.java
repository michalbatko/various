package pl.mbatko.type.general;

import java.util.ArrayList;
import java.util.List;

public class Drink {

    private DrinkType type;
    private List<DrinkAddOnType> addOns = new ArrayList<>();

    private Drink(DrinkType type) {
        this.type = type;
    }

    public static Drink create(DrinkType type) {
        return new Drink(type);
    }

    public DrinkType getType() {
        return type;
    }

    public Drink with(DrinkAddOnType addOn) {
        this.addOns.add(addOn);
        return this;
    }

    public List<DrinkAddOnType> getAddOns() {
        return addOns;
    }
}
