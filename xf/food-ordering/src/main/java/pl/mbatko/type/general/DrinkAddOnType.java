package pl.mbatko.type.general;

public class DrinkAddOnType extends SelectableType {

    public DrinkAddOnType(String code, String description) {
        super(code, description);
    }
}
