package pl.mbatko.type.general;

public abstract class DrinkType extends SelectableFoodType {

    public DrinkType(String code, String name, double price) {
        super(code, name, price);
    }
}
