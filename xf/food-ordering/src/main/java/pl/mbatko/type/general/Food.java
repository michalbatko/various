package pl.mbatko.type.general;

import pl.mbatko.other.Price;

public interface Food {

    String getName();

    Price getPrice();
}
