package pl.mbatko.type.general;

import java.util.Objects;

public class MealType extends SelectableFoodType {

    private CuisineType cuisine;

    public MealType(CuisineType cuisine, String code, String name, double price) {
        super(code, name, price);
        this.cuisine = cuisine;
    }

    public CuisineType getCuisine() {
        return cuisine;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        MealType mealType = (MealType) o;
        return Objects.equals(cuisine, mealType.cuisine);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), cuisine);
    }
}
