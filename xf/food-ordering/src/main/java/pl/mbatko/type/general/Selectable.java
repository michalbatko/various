package pl.mbatko.type.general;

public interface Selectable {

    String getDescription();

    String getCode();
}
