package pl.mbatko.type.general;

import pl.mbatko.other.Price;

import java.util.Objects;

public class SelectableFoodType extends SelectableType implements Food {

    private String name;
    private Price price;

    public SelectableFoodType(String code, String name, double price) {
        super(code, null);
        this.name = name;
        this.price = Price.of(price);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Price getPrice() {
        return price;
    }

    @Override
    public String getDescription() {
        return getName() + " - " + getPrice();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SelectableFoodType that = (SelectableFoodType) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(price, that.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name, price);
    }
}