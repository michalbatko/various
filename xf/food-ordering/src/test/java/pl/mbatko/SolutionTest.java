package pl.mbatko;

import org.junit.Test;
import pl.mbatko.dialog.Dialog;
import pl.mbatko.dialog.FakeDialog;
import pl.mbatko.menu.DefaultMenu;
import pl.mbatko.menu.Menu;
import pl.mbatko.order.OrderingSystem;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

public class SolutionTest {

    @Test
    public void shouldCollectOrder() {

        //given
        String[] inputSequence = {"y", "w", "y", "l", "/", "/", "y", "p", "s", "k", "/"};
        Dialog dialog = new FakeDialog(inputSequence);
        Menu menu = new DefaultMenu();

        OrderingSystem system = new OrderingSystem(dialog, menu);

        //when
        String summary = system.collectOrder().toString();

        //then
        assertThat(summary, containsString("Mineral water"));
        assertThat(summary, containsString("+ Lemon"));
        assertThat(summary, containsString("Kotlet schabowy"));
        assertThat(summary, containsString("Kremowka"));
        assertThat(summary, containsString("Total: 22.00"));
    }
}