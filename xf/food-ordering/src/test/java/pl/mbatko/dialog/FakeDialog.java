package pl.mbatko.dialog;

import pl.mbatko.type.general.Selectable;

import java.util.Optional;

public class FakeDialog implements Dialog {

    private Dialog dialog;

    public FakeDialog(String[] inputSequence) {
        this.dialog = new DefaultDialog(new CommandLine(inputSequence));
    }

    @Override
    public void message(String message) {
        dialog.message(message);
    }

    @Override
    public boolean question(String question) {
        return dialog.question(question);
    }

    @Override
    public <T extends Selectable> Optional<T> select(String question, T[] items) {
        return dialog.select(question, items);
    }
}
