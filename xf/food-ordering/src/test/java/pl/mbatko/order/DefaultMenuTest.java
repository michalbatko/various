package pl.mbatko.order;

import org.junit.Assert;
import org.junit.Test;
import pl.mbatko.menu.DefaultMenu;
import pl.mbatko.type.cuisine.PolishCuisine;
import pl.mbatko.type.general.*;

import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.fail;

public class DefaultMenuTest {

    DefaultMenu menu = new DefaultMenu();

    @Test
    public void shouldSelectDessertTypesForCuisine() {
        //given
        PolishCuisine polishCuisine = PolishCuisine.get();

        //when
        DessertType[] desserts = menu.getDesserts(polishCuisine);

        //then
        Stream.of(desserts).forEach(type -> Assert.assertThat(type.getCuisine(), equalTo(polishCuisine)));
    }

    @Test
    public void shouldSelectCourseTypesForCuisine() {
        //given
        PolishCuisine polishCuisine = PolishCuisine.get();

        //when
        CourseType[] courses = menu.getCourses(polishCuisine);

        //then
        Stream.of(courses).forEach(type -> Assert.assertThat(type.getCuisine(), equalTo(polishCuisine)));
    }

    @Test
    public void shouldHaveUniqueCodesForDrinks() {
        //when
        DrinkType[] types = menu.getDrinks();
        //then
        validateUniqueCodes(types);
    }

    @Test
    public void shouldHaveUniqueCodesForDrinkAddOns() {
        //when
        DrinkAddOnType[] types = menu.getDrinkAddOns();
        //then
        validateUniqueCodes(types);
    }

    @Test
    public void shouldHaveUniqueCodesForCuisines() {
        //when
        CuisineType[] types = menu.getCuisines();
        //then
        validateUniqueCodes(types);
    }

    @Test
    public void shouldHaveUniqueCodesForCourses() {
        //when
        CourseType[] drinks = menu.getCourses();
        //then
        validateUniqueCodes(drinks);
    }

    @Test
    public void shouldHaveUniqueCodesForDesserts() {
        //when
        DessertType[] drinks = menu.getDesserts();
        //then
        validateUniqueCodes(drinks);
    }

    private void validateUniqueCodes(Selectable[] selectables) {
        Map<String, Long> codeToOccurrence = Stream.of(selectables).collect(Collectors.groupingBy(Selectable::getCode, Collectors.counting()));
        Optional<Map.Entry<String, Long>> violation = codeToOccurrence.entrySet().stream().filter(entry -> entry.getValue() > 1).findFirst();

        if (violation.isPresent()) {
            String key = violation.get().getKey();
            fail("Non-unique code: " + key + " for: " + Arrays.toString(DefaultMenu.byCode(key, selectables)));
        }
    }
}