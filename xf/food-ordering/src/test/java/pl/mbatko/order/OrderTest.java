package pl.mbatko.order;

import org.junit.Test;
import pl.mbatko.type.course.Pizza;
import pl.mbatko.type.dessert.Tiramisu;
import pl.mbatko.type.drink.Lemon;
import pl.mbatko.type.drink.Water;
import pl.mbatko.type.general.Course;
import pl.mbatko.type.general.Dessert;
import pl.mbatko.type.general.Drink;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.junit.Assert.assertThat;

public class OrderTest {

    @Test
    public void shouldBuildOrderWithSummary() {

        //given
        Order order = new Order();

        Drink water = Drink.create(new Water()).with(new Lemon());
        Course pizza = Course.create(new Pizza());
        Dessert tiramisu = Dessert.create(new Tiramisu());

        //when
        order.addDrink(water);
        order.addCourse(pizza);
        order.addDessert(tiramisu);

        String summary = order.toString();

        //then
        assertThat(order.getDrinks(), hasItem(water));
        assertThat(order.getCourses(), hasItem(pizza));
        assertThat(order.getDesserts(), hasItem(tiramisu));

        assertThat(summary, containsString("Summary:"));
        assertThat(summary, containsString("Drinks:"));
        assertThat(summary, containsString("Mineral water"));
        assertThat(summary, containsString("+ Lemon"));
        assertThat(summary, containsString("Main courses:"));
        assertThat(summary, containsString("Pizza"));
        assertThat(summary, containsString("Desserts:"));
        assertThat(summary, containsString("Tiramisu"));
        assertThat(summary, containsString("Total: 19.00"));
    }
}