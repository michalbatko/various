package pl.mbatko.order;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.mbatko.dialog.Dialog;
import pl.mbatko.menu.Menu;
import pl.mbatko.type.course.Bigos;
import pl.mbatko.type.cuisine.PolishCuisine;
import pl.mbatko.type.dessert.Szarlotka;
import pl.mbatko.type.drink.Ice;
import pl.mbatko.type.drink.Water;
import pl.mbatko.type.general.*;

import java.util.Optional;

import static org.mockito.Mockito.*;
import static pl.mbatko.dialog.DialogConstants.*;

@RunWith(value = MockitoJUnitRunner.class)
public class OrderingSystemTest {

    @Mock
    Dialog dialog;

    @Mock
    Menu menu;

    @InjectMocks
    OrderingSystem system;

    @Test
    public void shouldCreateFullOrder() {
        //given
        TestDefaults def = new TestDefaults();
        setupMenu(def);
        setupDialog(def);

        //when
        Order order = system.collectOrder();

        //then
        verifyMenuInteractions(def);
        verifyDialogInteractions(def, order);
    }

    private void setupMenu(TestDefaults def) {
        when(menu.getDrinks()).thenReturn(def.drinks);
        when(menu.getDrinkAddOns()).thenReturn(def.drinkAddOns);
        when(menu.getCuisines()).thenReturn(def.cuisines);
        when(menu.getCourses(eq(def.polishCuisine))).thenReturn(def.courses);
        when(menu.getDesserts(eq(def.polishCuisine))).thenReturn(def.desserts);
    }

    private void setupDialog(TestDefaults def) {
        when(dialog.question(eq(NEXT_DRINK))).thenReturn(true, false);
        when(dialog.question(eq(NEXT_DRINK_ADD_ON))).thenReturn(true, false);
        when(dialog.question(eq(NEXT_MEAL))).thenReturn(true, false);

        when(dialog.select(eq(SELECT_DRINK), eq(def.drinks))).thenReturn(Optional.of(def.water));
        when(dialog.select(eq(SELECT_DRINK_ADD_ON), eq(def.drinkAddOns))).thenReturn(Optional.of(def.ice));
        when(dialog.select(eq(SELECT_CUISINE), eq(def.cuisines))).thenReturn(Optional.of(def.polishCuisine));
        when(dialog.select(eq(SELECT_COURSE), eq(def.courses))).thenReturn(Optional.of(def.bigos));
        when(dialog.select(eq(SELECT_DESSERT), eq(def.desserts))).thenReturn(Optional.of(def.szarlotka));
    }

    private void verifyMenuInteractions(TestDefaults def) {
        verify(menu).getDrinks();
        verify(menu).getDrinkAddOns();
        verify(menu).getCuisines();
        verify(menu).getCourses(def.polishCuisine);
        verify(menu).getDesserts(def.polishCuisine);

        verifyNoMoreInteractions(menu);
    }

    private void verifyDialogInteractions(TestDefaults def, Order order) {
        verify(dialog).message(WELCOME);
        verify(dialog).message(order.toString());
        verify(dialog, times(2)).question(NEXT_DRINK);
        verify(dialog, times(2)).question(NEXT_DRINK_ADD_ON);
        verify(dialog, times(2)).question(NEXT_MEAL);
        verify(dialog).select(SELECT_DRINK, def.drinks);
        verify(dialog).select(SELECT_DRINK_ADD_ON, def.drinkAddOns);
        verify(dialog).select(SELECT_CUISINE, def.cuisines);
        verify(dialog).select(SELECT_COURSE, def.courses);
        verify(dialog).select(SELECT_DESSERT, def.desserts);

        verifyNoMoreInteractions(dialog);
    }

    private class TestDefaults {
        PolishCuisine polishCuisine = PolishCuisine.get();
        Water water = new Water();
        Ice ice = new Ice();
        Bigos bigos = new Bigos();
        Szarlotka szarlotka = new Szarlotka();

        CuisineType[] cuisines = {polishCuisine};
        DrinkType[] drinks = {water};
        DrinkAddOnType[] drinkAddOns = {ice};
        CourseType[] courses = {bigos};
        DessertType[] desserts = {szarlotka};
    }
}