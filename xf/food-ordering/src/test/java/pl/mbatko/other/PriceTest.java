package pl.mbatko.other;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class PriceTest {

    @Test
    public void shouldAddPrices() throws Exception {
        //given
        Price first = Price.of(1.0);
        Price second = Price.of(0.99);
        //when
        first.add(second);
        //then
        assertThat(first.toString(), equalTo("1.99"));
    }
}