package pl.mbatko;

public class Main {

    public static void main(String[] args) {
        if (args.length == 1) {
            String mineField = args[0].replace("\\n", "\n");

            MineSweeper mineSweeper = new MineSweeperImpl(new MineProcessor());
            mineSweeper.setMineField(mineField);
            String hintField = mineSweeper.getHintField();

            System.out.println(hintField);
        } else {
            System.out.println("ERROR: mine field missing!");
        }
    }
}
