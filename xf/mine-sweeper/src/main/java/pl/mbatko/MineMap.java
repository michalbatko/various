package pl.mbatko;

import java.util.stream.IntStream;

/**
 * Represents map of mines.
 */
public class MineMap {

    private final int width;
    private final int height;

    private final int[][] hints;
    private final boolean[][] mines;

    public MineMap(int width, int height) {
        if (width <= 0 || height <= 0) {
            throw new IllegalArgumentException("Invalid dimensions of mine map");
        }
        this.width = width;
        this.height = height;
        this.hints = new int[width + 2][height + 2];
        this.mines = new boolean[width + 2][height + 2];
    }

    /**
     * Sets mine at given coordinates
     *
     * @param x horizontal coordinate (1-based index)
     * @param y vertical coordinate (1-based index)
     */
    public MineMap mine(int x, int y) {
        if (x > 0 && x <= width && y > 0 && y <= height) {
            mines[x][y] = true;
            IntStream.rangeClosed(x - 1, x + 1).forEach(w ->
                            IntStream.rangeClosed(y - 1, y + 1).forEach(h ->
                                            hints[w][h]++
                            )
            );
        } else {
            throw new IllegalArgumentException("Invalid index of mine map");
        }

        return this;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        IntStream.rangeClosed(1, height).forEach(y -> {
            builder.append("\n");
            IntStream.rangeClosed(1, width).forEach(x ->
                    builder.append(mines[x][y] ? "*" : hints[x][y]));
        });

        return builder.substring(1);
    }
}
