package pl.mbatko;

import java.util.Optional;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Produces map of mines
 */
public class MineProcessor {

    public Optional<MineMap> createMap(String mineField) throws IllegalArgumentException {
        if (mineField != null) {
            String[] lines = mineField.split("\n", -1);

            if (lines.length > 0 && lines[0].length() > 0) {
                int height = lines.length;
                int width = lines[0].length();

                if (validate(lines, width)) {
                    return Optional.of(getMineMap(lines, height, width));
                }
            }
        }

        throw new IllegalArgumentException("Mine field not properly formatted");
    }

    private MineMap getMineMap(String[] lines, int height, int width) {
        MineMap map = new MineMap(width, height);
        IntStream.range(0, height).forEach(y ->
                IntStream.range(0, width).forEach(x -> {
                    if (lines[y].charAt(x) == '*') {
                        map.mine(x + 1, y + 1);
                    }
                }));

        return map;
    }

    private boolean validate(String[] lines, int width) {
        String pattern = "[*.]{" + width + "}";
        return Stream.of(lines).allMatch(line -> line.matches(pattern));
    }
}
