package pl.mbatko;

import java.util.Optional;

public class MineSweeperImpl implements MineSweeper {

    private final MineProcessor processor;
    private Optional<MineMap> map = Optional.empty();

    public MineSweeperImpl(MineProcessor processor) {
        this.processor = processor;
    }

    @Override
    public void setMineField(String mineField) throws IllegalArgumentException {
        this.map = processor.createMap(mineField);
    }

    @Override
    public String getHintField() throws IllegalStateException {
        if (!map.isPresent()) {
            throw new IllegalStateException("Mine field not initialised properly!");
        }

        return map.get().toString();
    }
}
