package pl.mbatko

import spock.lang.Specification

class MineMapTest extends Specification {

    def "should produce valid map"() {
        given:
        MineMap map = new MineMap(2,2)

        when:
        map.mine(1,1).mine(2,2)

        then:
        map.toString() == "*2\n2*"
    }

    def "should produce one cell map"() {
        given:
        MineMap map = new MineMap(1,1)

        when:
        map.mine(1,1)

        then:
        map.toString() == "*"
    }

    def "should report invalid dimensions"() {
        when:
        new MineMap(-1,0)

        then:
        thrown(IllegalArgumentException)
    }

    def "should report invalid coordinates"() {
        given:
        MineMap map = new MineMap(2,2)

        when:
        map.mine(0,1)

        then:
        thrown(IllegalArgumentException)
    }
}

