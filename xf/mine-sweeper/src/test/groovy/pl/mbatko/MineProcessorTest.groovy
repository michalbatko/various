package pl.mbatko

import spock.lang.Specification
import spock.lang.Unroll

class MineProcessorTest extends Specification {

    MineProcessor processor = new MineProcessor();

    @Unroll
    def "should produce valid map"() {
        when:
        Optional<MineMap> map = processor.createMap(validMineField)

        then:
        map.isPresent()
        map.get().toString() == mineMap

        where:
        validMineField     | mineMap
        ".*\n.."           | "1*\n11"
        "*"                | "*"
        "."                | "0"
        ".*.."             | "1*10"
        ".\n*\n.\n."       | "1\n*\n1\n0"
        "..\n.."           | "00\n00"
        "*...\n..*.\n...." | "*211\n12*1\n0111"
    }

    @Unroll("should throw exception when invalid mine field (#description)")
    def "should throw exception when invalid mine field"() {
        when:
        processor.createMap(invalidMineField);

        then:
        thrown(IllegalArgumentException)

        where:
        description       | invalidMineField
        "null"            | null
        "empty"           | ""
        "wrong format"    | "..\n*\n.."
        "new line"        | "\n"
        "new line before" | "\n..\n**"
        "new line after"  | "..\n**\n"
        "double new line" | "..\n\n.."
    }
}
