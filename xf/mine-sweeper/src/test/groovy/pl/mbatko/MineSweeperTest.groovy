package pl.mbatko

import spock.lang.Specification

class MineSweeperTest extends Specification {

    MineSweeper mineSweeper = new MineSweeperImpl(new MineProcessor())

    def "should produce valid hints"() {
        when:
        mineSweeper.setMineField(mineField)

        then:
        mineSweeper.getHintField() == hintField

        where:
        mineField          | hintField
        "*"                | "*"
        "."                | "0"
        ".*.."             | "1*10"
        "*...\n..*.\n...." | "*211\n12*1\n0111"
    }

    def "should throw exception when invalid mine field"() {
        given:
        String invalid = "*..\n.*.\n."

        when:
        mineSweeper.setMineField(invalid);

        then:
        thrown(IllegalArgumentException)
    }

    def "should throw exception when not initialized"() {
        when:
        mineSweeper.getHintField()

        then:
        thrown(IllegalStateException)
    }
}

