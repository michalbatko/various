# 1/
SELECT
    c.Name
FROM
    xf.country c JOIN xf.city ct ON c.CountryID = ct.CountryID
GROUP BY
    c.Name
HAVING
    SUM(ct.Population) > 400;


# 2/
SELECT
    co.Name
FROM
    xf.country co
WHERE
    co.CountryID NOT IN (
        SELECT
            c.CountryID
        FROM
            xf.country c JOIN xf.city t ON c.CountryID = t.CountryID JOIN xf.building b ON t.CityID = b.CityID);